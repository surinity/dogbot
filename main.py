from flask import Flask, render_template, redirect, request
from flask_sqlalchemy import SQLAlchemy
import os
import json
import datetime
from slackclient import SlackClient

SLACK_BOT_TOKEN = os.environ["SLACK_BOT_TOKEN"]
sc = SlackClient(SLACK_BOT_TOKEN)
doggoUser = 'UC7C5R6BY'
atDoggo = '<@UC7C5R6BY>'
blendogs = '#doghouse'

app = Flask(__name__)
app.config['DEBUG'] = True

basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'dog.db')
db = SQLAlchemy(app)


pacificTimeDelta = datetime.timedelta(hours=7)
fmt = '%B %d'

def today():
    return datetime.datetime.now() - pacificTimeDelta

class Dogsitter(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True)
    canDogsit = db.Column(db.Boolean)

    def __init__(self, name, canDogsit):
        self.name = name
        self.canDogsit = canDogsit

def dogsit(name, canDogsit):
    sitter = Dogsitter.query.filter_by(name=name).first()
    if sitter:
        sitter.canDogsit = canDogsit
        db.session.commit()
    else:
        sitter = Dogsitter(name, canDogsit)
        db.session.add(sitter)
        db.session.commit()


class Dog(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    human = db.Column(db.String(80))
    breed = db.Column(db.String(80))
    gender = db.Column(db.String(80))
    favoriteThing = db.Column(db.Text)
    funnyQuirk = db.Column(db.Text)
    info = db.Column(db.Text)

    lastVisit = db.Column(db.DateTime)
    feedingInstructions = db.Column(db.Text)

    def __init__(self, name, human):
        self.name = name
        self.human = human

def create(name, human):
    dog = Dog(name.lower(), human)
    db.session.add(dog)
    db.session.commit()

def getDog(name = ''):
    n = name.lower()
    return Dog.query.filter_by(name=n).first()

def getHumans():
    allDogs = Dog.query.all()
    for d in allDogs:
        print d.human

def update(name, human=None, breed=None, gender=None, favoriteThing=None, funnyQuirk=None, info=None, feedingInstructions=None, inToday=False, notInToday=True):
    dog = getDog(name)
    if dog:
        if human:
            dog.human = human
        if breed:
            dog.breed = breed
        if gender:
            dog.gender = gender
        if favoriteThing:
            dog.favoriteThing = favoriteThing
        if funnyQuirk:
            dog.funnyQuirk = funnyQuirk
        if info:
            dog.info = info
        if feedingInstructions:
            dog.feedingInstructions = feedingInstructions
        if inToday:
            dog.lastVisit = today()
        if not notInToday:
            dog.lastVisit = None
        db.session.commit()


def speak(message, channel):
    if channel[0] == 'C':
        message = message.replace('<', '')
        message = message.replace('>', '')
    sc.api_call(
        'chat.postMessage',
        channel=channel,
        text=message,
    )
    return 'woof', 200

def missingDog(name, channel):
    return speak("I'm sorry, I don't know who " + name.lower().capitalize() + " is", channel)

def dogsInToday(channel):
    allDogs = Dog.query.all()
    presentDogs = []
    t = today().date()
    for d in allDogs:
        if d.lastVisit and d.lastVisit.date() == t:
            presentDogs.append(d.name.capitalize())
    if presentDogs:
        return speak('\n'.join(presentDogs), channel)
    else:
        return speak('No one has said their dog is in today ):', channel)

def greeting(channel):
    message = ("Arf! Here are some things you can ask me: " +
               "\n- What dogs are in today?" +
               "\n- Can I feed Olive?" +
               "\n- Charlie's profile" +
               "\nOr check out my handbook:" +
               "\nhttps://blendlabs.atlassian.net/wiki/spaces/BLEND/pages/392299747/DogBot")
    return speak(message, channel)

def profile(name, channel):
    if name and name.endswith("'s"):
        name = name[:-2]

    dog = getDog(name)

    if not dog:
        return missingDog(name, channel)
    else:
        message = 'About ' + name.capitalize() + ':'
        if name.lower() == 'bailey':
            message = message + "\nHuman: Bailey's human is <@nima>, but <" + dog.human + "> looks after her in the office"
        else:
            message = message + '\nHuman: <' + dog.human + '>'
        if dog.breed:
            message = message + '\nBreed: ' + dog.breed
        if dog.gender:
            message = message + '\nGender: ' + dog.gender
        if dog.favoriteThing:
            message = message + '\nFavorite thing: ' + dog.favoriteThing
        if dog.funnyQuirk:
            message = message + '\nFunny Quirk: ' + dog.funnyQuirk
        if dog.info:
            message = message + '\nExtra Info: ' + dog.info
        return speak(message, channel)

@app.route('/', methods=['GET', 'POST'])
def doggo():
    data = json.loads(request.data)
    event = data['event']

    channel = event['channel']
    channel_type = event['channel_type']
    text = event['text']
    textLower = text.lower()
    sanitized = text.replace(atDoggo, '')
    user = event['user']
    print text

    if atDoggo in text or channel_type == 'app_home' and user != doggoUser:
        if "i'm interested in dogsitting" in textLower:
            dogsit(user, True)
            return speak("Thanks! I'll add you to the list", channel)
        elif "i'm not interested in dogsitting" in textLower:
            dogsit(user, False)
            return speak("Sorry to hear that. I'll take you off the list", channel)
        elif "who's interested in dogsitting" in textLower:
            allSitters = Dogsitter.query.all()
            available = []
            for d in allSitters:
                if d.canDogsit:
                    available.append("<@" + d.name + ">")
            if available:
                return speak('These people are interested in dogsitting:\n' + '\n'.join(available), channel)
            else:
                return speak('No one has expressed interested in dogsitting', channel)
        elif "dogsit" in textLower:
            return speak("Try one of these phrases:\n`I'm interested in dogsitting`\n`I'm not interested in dogsitting`\n`Who's interested in dogsitting?`", channel)
        elif 'pooped' in textLower:
            phrase = textLower.split()
            poopedIndex = phrase.index('pooped')
            name = phrase[poopedIndex - 1]

            if name and name.lower() == 'someone':
                return speak(":alert: :poop: Oopsie! There's been an accident\n> " + sanitized + "\nThere's a clean-up kit in the copy area of each floor", blendogs)

            dog = getDog(name)

            if not dog:
                return missingDog(name, channel)
            else:
                human = dog.human
                speak("Oh no! Thanks for letting me know! I'll let <" + human + "> know", channel)
                return speak("Someone reported:\n> " + sanitized, human)
        elif 'peed' in textLower:
            phrase = textLower.split()
            peedIndex = phrase.index('peed')
            name = phrase[peedIndex - 1]

            if name and name.lower() == 'someone':
                return speak(":alert: :poop: Oopsie! There's been an accident\n> " + sanitized + "\nThere's a clean-up kit in the copy area of each floor", blendogs)

            dog = getDog(name)

            if not dog:
                return missingDog(name, channel)
            else:
                human = dog.human
                speak("Oh no! Thanks for letting me know! I'll let <" + human + "> know", channel)
                return speak("Someone reported:\n> " + sanitized, human)
        elif 'there is poop' in textLower or "there's poop" in textLower or 'there is pee' in textLower or "there's pee" in textLower:
            return speak(":alert: :poop: Oopsie! There's been an accident\n> " + sanitized + "\nThere's a clean-up kit in the copy area of each floor", blendogs)
        elif 'directory' in text:
            return speak("You can find the dog directory here: https://blendlabs.atlassian.net/wiki/spaces/PEOPLE/pages/70922241/Dogs+of+Blend+Directory", channel)
        elif 'favorite thing' in textLower:
            phrase = textLower.split()
            favoriteIndex = phrase.index('favorite')
            name = phrase[favoriteIndex - 1]

            if name and name.endswith("'s"):
                name = name[:-2]

            dog = getDog(name)

            if not dog:
                return missingDog(name, channel)
            elif dog.favoriteThing:
                message = name + "'s human says:\n> " + dog.favoriteThing
                return speak(message, channel)
            else:
                return speak("I don't know what " + name + "'s favorite thing is")
        elif 'feed ' in textLower:
            phrase = textLower.split()
            feedIndex = phrase.index('feed')
            name = phrase[feedIndex + 1]

            if name and name.endswith('?'):
                name = name[:-1]

            dog = getDog(name)

            if not dog:
                return missingDog(name, channel)
            elif dog.feedingInstructions:
                message = name.lower().capitalize() + "'s human says:\n> " + dog.feedingInstructions
                return speak(message, channel)
            else:
                message = "I don't know. You can ask " + name.lower().capitalize() + "'s human <" + dog.human + ">"
                return speak(message, channel)
        elif "what dogs are in today" in textLower:
            return dogsInToday(channel)
        elif "what dogs are in the office today" in textLower:
            return dogsInToday(channel)
        elif "what dogs are here today" in textLower:
            return dogsInToday(channel)
        elif "who's in today" in textLower:
            return dogsInToday(channel)
        elif "who is in today" in textLower:
            return dogsInToday(channel)
        elif "who's here today" in textLower:
            return dogsInToday(channel)
        elif "who is here today" in textLower:
            return dogsInToday(channel)
        elif "who's in the office today" in textLower:
            return dogsInToday(channel)
        elif "who is in the office today" in textLower:
            return dogsInToday(channel)
        elif ' in today' in textLower:
            phrase = textLower.split()
            inIndex = phrase.index('in')
            previous = phrase[inIndex - 1]

            if previous == 'is':
                name = phrase[inIndex - 2].lower()
                dog = getDog(name)

                if not dog:
                    return missingDog(name, channel)
                else:
                    update(name, inToday=True)
                    return speak("I'm excited to play with " + name.capitalize() + " today!", channel)
            elif previous == 'not':
                name = phrase[inIndex - 3]
                dog = getDog(name)

                if not dog:
                    return missingDog(name, channel)
                else:
                    update(name, notInToday=False)
                    return speak("Thanks for letting me know", channel)
            else:
                name = previous
                dog = getDog(name)

                if not dog:
                    return missingDog(name, channel)
                else:
                    lastVisit = dog.lastVisit
                    if lastVisit:
                        t = today()
                        if lastVisit.date() == t.date():
                            message = name.capitalize() + ' is here today!'
                            return speak(message, channel)
                        else:
                            human = dog.human
                            message = name.capitalize() + " was last reported in on " + lastVisit.strftime('%B %d') + ". Ask <" + human + ">"
                            return speak(message, channel)
                    else:
                        human = dog.human
                        message = "I don't know the last time " + name.capitalize() + " was in. Ask <" + human + ">"
                        return speak(message, channel)
        elif ' here today' in textLower:
            phrase = textLower.split()
            hereIndex = phrase.index('here')
            previous = phrase[hereIndex - 1]

            if previous == 'is':
                name = phrase[hereIndex - 2].lower()
                dog = getDog(name)

                if not dog:
                    return missingDog(name, channel)
                else:
                    update(name, inToday=True)
                    return speak("I'm excited to play with " + name.capitalize() + " today!", channel)
            elif previous == 'not':
                name = phrase[inIndex - 3]
                dog = getDog(name)

                if not dog:
                    return missingDog(name, channel)
                else:
                    update(name, notInToday=False)
                    return speak("Thanks for letting me know", channel)
            else:
                name = previous
                dog = getDog(name)

                if not dog:
                    return missingDog(name, channel)
                else:
                    lastVisit = dog.lastVisit
                    if lastVisit:
                        t = today()
                        if lastVisit.date() == t.date():
                            message = name.capitalize() + ' is here today!'
                            return speak(message, channel)
                        else:
                            human = dog.human
                            message = name.capitalize() + " was last reported in on " + lastVisit.strftime('%B %d') + ". Ask <" + human + ">"
                            return speak(message, channel)
                    else:
                        human = dog.human
                        message = "I don't know the last time " + name.capitalize() + " was in. Ask <" + human + ">"
                        return speak(message, channel)
        elif "profile" in textLower:
            phrase = textLower.split()
            profileIndex = phrase.index('profile')
            name = phrase[profileIndex - 1]

            return profile(name, channel)
        elif "bio" in textLower:
            phrase = textLower.split()
            bioIndex = phrase.index('bio')
            name = phrase[bioIndex - 1]

            return profile(name, channel)
        elif "who's" in textLower and "human" in textLower:
            phrase = textLower.split()
            whosIndex = phrase.index("who's")
            name = phrase[whosIndex + 1]

            if name and name.endswith("'s"):
                name = name[:-2]

            dog = getDog(name)

            if not dog:
                return missingDog(name, channel)
            else:
                if name.lower() == 'bailey':
                    return speak("Bailey's human is <@nima>, but <" + dog.human + "> looks after her in the office", channel)
                else:
                    return speak(dog.name.capitalize() + "'s human is <" + dog.human + ">", channel)
        elif "who is" in textLower and "human" in textLower:
            phrase = textLower.split()
            isIndex = phrase.index('is')
            name = phrase[isIndex + 1]

            if name and name.endswith("'s"):
                name = name[:-2]

            dog = getDog(name)

            if not dog:
                return missingDog(name, channel)
            else:
                if name.lower() == 'bailey':
                    return speak("Bailey's human is <@nima>, but <" + dog.human + "> looks after her in the office", channel)
                else:
                    return speak(dog.name.capitalize() + "'s human is <" + dog.human + ">", channel)
        elif "help" in textLower:
            message = ("Here's a sample list of commands that I know:" +
                       "\n`<Dog>` is in today" +
                       "\nIs `<Dog>` in today?" +
                       "\nWhat dogs are in today?" +
                       "\nWho is `<Dog>`'s human?" +
                       "\nIs there a dog directory?" +
                       "\n`<Dog>`'s profile" +
                       "\nWhat is `<Dog>`'s favorite thing?" +
                       "\n`<Dog>` pooped on the stadium" +
                       "\nThere's pee by the elevators on 3" +
                       "\nI'm interested in dogsitting" +
                       "\nI'm not interested in dogsitting" +
                       "\nWho's interested in dogsitting?" +
                       "\nSpeak" +
                       "\nOr check out my handbook:" +
                       "\nhttps://blendlabs.atlassian.net/wiki/spaces/BLEND/pages/392299747/DogBot")
            return speak(message, channel)
        elif "thanks" in textLower or "thank you" in textLower:
            return speak("You're welcome! What are best friends for? :dog:", channel)
        elif "speak" == textLower or "speak!" == textLower or (atDoggo + " speak").lower() in textLower:
            return speak('woof!', channel)
        elif "woof" == textLower or "woof!" == textLower or (atDoggo + " woof").lower() in textLower:
            return speak('woof!', channel)
        elif "bark" == textLower or "bark!" == textLower or (atDoggo + " bark").lower() in textLower:
            return speak('woof!', channel)
        elif "hello" == textLower or "hello!" == textLower or (atDoggo + " hello").lower() in textLower:
            return greeting(channel)
        elif "hi" == textLower or "hi!" == textLower or (atDoggo + " hi").lower() in textLower:
            return greeting(channel)
        elif "hey" == textLower or "hey!" == textLower or (atDoggo + " hey").lower() in textLower:
            return greeting(channel)
        elif "arf" == textLower or "arf!" == textLower or (atDoggo + " arf").lower() in textLower:
            return speak('arf!', channel)
        elif "shake" == textLower or (atDoggo + " shake").lower() in textLower:
            return speak(':dancingdog:', channel)
        elif "good girl" == textLower or (atDoggo + " good girl").lower() in textLower:
            return speak(':happy_dog:', channel)
        elif "good boy" == textLower or (atDoggo + " good boy").lower() in textLower:
            return speak(':happy_dog:', channel)
        elif "paw" == textLower or (atDoggo + " paw").lower() in textLower:
            return speak(':highfive:', channel)
        elif "shake paw" == textLower or (atDoggo + " shake paw").lower() in textLower:
            return speak(':highfive:', channel)
        elif "high five" == textLower or (atDoggo + " high five").lower() in textLower:
            return speak(':highfive:', channel)
        elif "play dead" == textLower or (atDoggo + " play dead").lower() in textLower:
            return speak(':mask: :dizzy_face: :skull_and_crossbones: :coffin:', channel)
        else:
            message = ("I don't know that command yet. Why don't you try one of these?:" +
                       "\n- What is Olive's favorite thing?" +
                       "\n- Who is Boo's human?" +
                       "\n- Can I feed Bingo?" +
                       "\n- I'm interested in dogsitting" +
                       "\nOr check out my handbook:" +
                       "\nhttps://blendlabs.atlassian.net/wiki/spaces/BLEND/pages/392299747/DogBot")
            return speak(message, channel)

    return 'Woof!', 200


if __name__ == '__main__':
    app.run(host='0.0.0.0')
